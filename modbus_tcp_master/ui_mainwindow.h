/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_4;
    QPushButton *historyBtn;
    QLineEdit *iplineEdit;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *labelGN;
    QLineEdit *start_ad_LineEdit;
    QLabel *labelstartaddrees;
    QPushButton *setupbtn;
    QLabel *labelslaveaddress;
    QPushButton *coilsetepbtn;
    QLineEdit *slave_LineEdit;
    QPushButton *regsetupbtn;
    QComboBox *GN_combox;
    QLabel *labelcoil;
    QLineEdit *coil_LineEdit;
    QPushButton *send_msg;
    QLineEdit *reg_LineEdit;
    QLabel *labelreg;
    QLineEdit *portlineEdit;
    QPushButton *connectbtn;
    QLabel *label;
    QLabel *label_2;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_7;
    QTableWidget *table_coil;
    QWidget *tab_2;
    QGridLayout *gridLayout_6;
    QTableWidget *tableregs;
    QPushButton *clearbtn;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTextEdit *show_Edit;
    QPushButton *transfiles;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(879, 891);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_4 = new QGridLayout(centralwidget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        historyBtn = new QPushButton(centralwidget);
        historyBtn->setObjectName(QString::fromUtf8("historyBtn"));

        gridLayout_4->addWidget(historyBtn, 0, 7, 1, 1);

        iplineEdit = new QLineEdit(centralwidget);
        iplineEdit->setObjectName(QString::fromUtf8("iplineEdit"));

        gridLayout_4->addWidget(iplineEdit, 0, 1, 1, 1);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        labelGN = new QLabel(groupBox_2);
        labelGN->setObjectName(QString::fromUtf8("labelGN"));

        gridLayout_2->addWidget(labelGN, 0, 0, 1, 1);

        start_ad_LineEdit = new QLineEdit(groupBox_2);
        start_ad_LineEdit->setObjectName(QString::fromUtf8("start_ad_LineEdit"));

        gridLayout_2->addWidget(start_ad_LineEdit, 2, 2, 1, 1);

        labelstartaddrees = new QLabel(groupBox_2);
        labelstartaddrees->setObjectName(QString::fromUtf8("labelstartaddrees"));

        gridLayout_2->addWidget(labelstartaddrees, 2, 0, 1, 1);

        setupbtn = new QPushButton(groupBox_2);
        setupbtn->setObjectName(QString::fromUtf8("setupbtn"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(setupbtn->sizePolicy().hasHeightForWidth());
        setupbtn->setSizePolicy(sizePolicy);
        setupbtn->setMaximumSize(QSize(200, 16777215));

        gridLayout_2->addWidget(setupbtn, 5, 2, 1, 1);

        labelslaveaddress = new QLabel(groupBox_2);
        labelslaveaddress->setObjectName(QString::fromUtf8("labelslaveaddress"));

        gridLayout_2->addWidget(labelslaveaddress, 1, 0, 1, 1);

        coilsetepbtn = new QPushButton(groupBox_2);
        coilsetepbtn->setObjectName(QString::fromUtf8("coilsetepbtn"));

        gridLayout_2->addWidget(coilsetepbtn, 6, 2, 1, 1);

        slave_LineEdit = new QLineEdit(groupBox_2);
        slave_LineEdit->setObjectName(QString::fromUtf8("slave_LineEdit"));

        gridLayout_2->addWidget(slave_LineEdit, 1, 2, 1, 1);

        regsetupbtn = new QPushButton(groupBox_2);
        regsetupbtn->setObjectName(QString::fromUtf8("regsetupbtn"));

        gridLayout_2->addWidget(regsetupbtn, 7, 2, 1, 1);

        GN_combox = new QComboBox(groupBox_2);
        GN_combox->addItem(QString());
        GN_combox->addItem(QString());
        GN_combox->addItem(QString());
        GN_combox->addItem(QString());
        GN_combox->setObjectName(QString::fromUtf8("GN_combox"));
        GN_combox->setEditable(false);

        gridLayout_2->addWidget(GN_combox, 0, 2, 1, 1);

        labelcoil = new QLabel(groupBox_2);
        labelcoil->setObjectName(QString::fromUtf8("labelcoil"));

        gridLayout_2->addWidget(labelcoil, 3, 0, 1, 1);

        coil_LineEdit = new QLineEdit(groupBox_2);
        coil_LineEdit->setObjectName(QString::fromUtf8("coil_LineEdit"));

        gridLayout_2->addWidget(coil_LineEdit, 3, 2, 1, 1);

        send_msg = new QPushButton(groupBox_2);
        send_msg->setObjectName(QString::fromUtf8("send_msg"));

        gridLayout_2->addWidget(send_msg, 9, 2, 1, 1);

        reg_LineEdit = new QLineEdit(groupBox_2);
        reg_LineEdit->setObjectName(QString::fromUtf8("reg_LineEdit"));

        gridLayout_2->addWidget(reg_LineEdit, 4, 2, 1, 1);

        labelreg = new QLabel(groupBox_2);
        labelreg->setObjectName(QString::fromUtf8("labelreg"));

        gridLayout_2->addWidget(labelreg, 4, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_2, 2, 0, 1, 4);

        portlineEdit = new QLineEdit(centralwidget);
        portlineEdit->setObjectName(QString::fromUtf8("portlineEdit"));

        gridLayout_4->addWidget(portlineEdit, 0, 3, 1, 1);

        connectbtn = new QPushButton(centralwidget);
        connectbtn->setObjectName(QString::fromUtf8("connectbtn"));

        gridLayout_4->addWidget(connectbtn, 0, 4, 1, 1);

        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        gridLayout_4->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        gridLayout_4->addWidget(label_2, 0, 2, 1, 1);

        groupBox_4 = new QGroupBox(centralwidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        tabWidget = new QTabWidget(groupBox_4);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_7 = new QGridLayout(tab);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        table_coil = new QTableWidget(tab);
        if (table_coil->columnCount() < 65536)
            table_coil->setColumnCount(65536);
        if (table_coil->rowCount() < 2)
            table_coil->setRowCount(2);
        table_coil->setObjectName(QString::fromUtf8("table_coil"));
        table_coil->setRowCount(2);
        table_coil->setColumnCount(65536);

        gridLayout_7->addWidget(table_coil, 1, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_6 = new QGridLayout(tab_2);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        tableregs = new QTableWidget(tab_2);
        if (tableregs->columnCount() < 65536)
            tableregs->setColumnCount(65536);
        if (tableregs->rowCount() < 2)
            tableregs->setRowCount(2);
        tableregs->setObjectName(QString::fromUtf8("tableregs"));
        tableregs->setRowCount(2);
        tableregs->setColumnCount(65536);

        gridLayout_6->addWidget(tableregs, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout_3->addWidget(tabWidget, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_4, 3, 0, 1, 8);

        clearbtn = new QPushButton(centralwidget);
        clearbtn->setObjectName(QString::fromUtf8("clearbtn"));

        gridLayout_4->addWidget(clearbtn, 0, 5, 1, 1);

        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        show_Edit = new QTextEdit(groupBox);
        show_Edit->setObjectName(QString::fromUtf8("show_Edit"));
        QFont font;
        font.setFamily(QString::fromUtf8("Adobe \346\245\267\344\275\223 Std R"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        show_Edit->setFont(font);
        show_Edit->setReadOnly(true);

        gridLayout->addWidget(show_Edit, 1, 0, 1, 1);


        gridLayout_4->addWidget(groupBox, 1, 0, 1, 8);

        transfiles = new QPushButton(centralwidget);
        transfiles->setObjectName(QString::fromUtf8("transfiles"));

        gridLayout_4->addWidget(transfiles, 0, 6, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 879, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        GN_combox->setCurrentIndex(0);
        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        historyBtn->setText(QCoreApplication::translate("MainWindow", "\344\277\235\345\255\230\345\216\206\345\217\262\350\256\260\345\275\225", nullptr));
        iplineEdit->setText(QCoreApplication::translate("MainWindow", "10.100.14.37", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "\350\256\276\347\275\256", nullptr));
        labelGN->setText(QCoreApplication::translate("MainWindow", "\345\212\237\350\203\275\347\240\201\357\274\232", nullptr));
        start_ad_LineEdit->setPlaceholderText(QCoreApplication::translate("MainWindow", "\350\257\267\350\276\223\345\205\245\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232(0-65535)", nullptr));
        labelstartaddrees->setText(QCoreApplication::translate("MainWindow", "\350\265\267\346\227\266\345\234\260\345\235\200\357\274\232", nullptr));
        setupbtn->setText(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201", nullptr));
        labelslaveaddress->setText(QCoreApplication::translate("MainWindow", "\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232", nullptr));
        coilsetepbtn->setText(QCoreApplication::translate("MainWindow", "0F\345\217\202\346\225\260\350\256\276\347\275\256", nullptr));
        slave_LineEdit->setPlaceholderText(QCoreApplication::translate("MainWindow", "\350\257\267\350\276\223\345\205\245\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232(1-247)", nullptr));
        regsetupbtn->setText(QCoreApplication::translate("MainWindow", "10\345\217\202\346\225\260\350\256\276\347\275\256", nullptr));
        GN_combox->setItemText(0, QCoreApplication::translate("MainWindow", "01", nullptr));
        GN_combox->setItemText(1, QCoreApplication::translate("MainWindow", "03", nullptr));
        GN_combox->setItemText(2, QCoreApplication::translate("MainWindow", "10", nullptr));
        GN_combox->setItemText(3, QCoreApplication::translate("MainWindow", "0F", nullptr));

        GN_combox->setCurrentText(QCoreApplication::translate("MainWindow", "01", nullptr));
        labelcoil->setText(QCoreApplication::translate("MainWindow", "\347\272\277\345\234\210\344\270\252\346\225\260:", nullptr));
        coil_LineEdit->setPlaceholderText(QCoreApplication::translate("MainWindow", "\350\257\267\350\276\223\345\205\245\347\272\277\345\234\210\346\225\260\351\207\217(1-2000)", nullptr));
        send_msg->setText(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201", nullptr));
        reg_LineEdit->setPlaceholderText(QCoreApplication::translate("MainWindow", "\350\257\267\350\276\223\345\205\245\345\257\204\345\255\230\345\231\250\346\225\260\351\207\217(1-125)", nullptr));
        labelreg->setText(QCoreApplication::translate("MainWindow", "\345\257\204\345\255\230\345\231\250\344\270\252\346\225\260\357\274\232", nullptr));
        portlineEdit->setText(QCoreApplication::translate("MainWindow", "502", nullptr));
        connectbtn->setText(QCoreApplication::translate("MainWindow", "\350\277\236\346\216\245", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "IP:", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "\347\253\257\345\217\243\357\274\232", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "\346\230\276\347\244\272", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "\347\272\277\345\234\210", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "\345\257\204\345\255\230\345\231\250", nullptr));
        clearbtn->setText(QCoreApplication::translate("MainWindow", "\346\270\205\351\231\244", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "\346\266\210\346\201\257\346\230\276\347\244\272", nullptr));
        transfiles->setText(QCoreApplication::translate("MainWindow", "\344\274\240\350\276\223\346\226\207\344\273\266", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

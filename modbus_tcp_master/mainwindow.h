#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "dialog0f.h"
#include "dialog10.h"
#include "widget.h"

#include<QMainWindow>
#include<QTcpSocket>
#include<QMessageBox>
#include<QHostAddress>
#include<QThread>
#include<QTimer>
#include<QDateTime>
#include<QFile>
#include<QFileDialog>
#include<QCloseEvent>
#include<QSettings>

#define MB_PROTOCOL            0x0000

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Dialog0f *dialog0f=nullptr;//0f功能码
    Dialog10 *dialog10=nullptr;//10功能码
    Widget *widget = nullptr;   //文件传输
    QByteArray getUserInputData();

signals:

public slots:


private slots:
     void hide_SLOTS();
     void table_dataInit();
     void wirtTablec(quint16 num, quint16 satrtaddr, QString bac);
     void wirtTabler(quint16 num, quint16 satrtaddr, QVector<quint16> bar);
     //主线程初始化
     void init();
     void ReadOutIniData();
     void on_coilsetepbtn_clicked();
     void on_regsetupbtn_clicked();
     void on_clearbtn_clicked();
     void on_connectbtn_clicked();
     void on_send_msg_clicked();
     void showMsg(QString msg);
     void m_ReadMsg();
     void ClientReadError(QAbstractSocket::SocketError);
     void packageSartc(quint8 addr,quint8 funcode,quint16 startaddr,quint16 num,QByteArray ba,QVector<quint16>regs);
     bool TCPAnalysisMessage(QByteArray MessageArray);
    void resendShow();
    void on_setupbtn_clicked();


    void on_transfiles_clicked();

    void on_historyBtn_clicked();

private:
    Ui::MainWindow *ui;
    quint8 msgbuf[256];
    quint8 mb_identifier=1;
    quint8 delay_flag = 0;
    quint8 mb_lenght=0;
    QVector<quint16> bar;
    MainWindow * mb_msg;
    bool sendflag = false;
    bool tcpconnectflag=false;
    //初始化超时重传次数
    int resendNumber=0;
    QTimer * m_resendtimer;
    QByteArray requestMessage;
    QTcpSocket * m_TcpChile = nullptr;
    QByteArray bamsg;
    QSettings* readseting=nullptr;
    quint8 m_code=0xff;
    QVector<quint16> inf;
    QByteArray coildata;
    QByteArray resenddata;
    QVector<quint16> regdata;

//-----------------函数------------------
    //隐藏控件槽函数
    bool judgement(QString msg);
    bool checkUserInputData();
    bool MessageLegalJudge(QByteArray MessageArr, QByteArray requestMessageArr);
    //功能码01解析
    bool analysis01(QByteArray MessageArr, QByteArray requestMessageArr);
    //功能码03解析
    bool analysis03(QByteArray MessageArr, QByteArray requestMessageArr);
    //功能码0f解析
    bool analysis0f(QByteArray MessageArr, QByteArray requestMessageArr);
    //功能码10解析
    bool analysis10(QByteArray MessageArr, QByteArray requestMessageArr);
    //显示信息
    void showlogmsg(QString msg);
    //将读取的线圈数据写入ini文件
    void WirteCoilDataToIni(quint16 satrt, QString CoilData);
    //将读取的寄存器数据写入ini文件
    void WirteRegsDataToIni(quint16 startaddr, quint16 num, QVector<quint16> ba);
    // 异常码判断函数
    void TCPExceptionCodeJudge(quint8 excCode);
    //异常码报文处理函数
    bool TCPExceptionCodeProcess(QByteArray MessageArr);
    // 功能码合法性判断函数
    bool FuncCodeLegalJudge(QByteArray MessageArr);
    //字节反转函数
    void byteReverse(QString &coils);
    //byte转string
    void ByteToHexString(QString &str, QByteArray &ba);
    void m_SendMsg(QByteArray ba);
    void funCode0f(quint8 addr, quint16 startaddr, quint16 num, QByteArray bas);
    void funCode10(quint8 addr, quint16 startaddr, QVector<quint16> addrvalue);
    void ReadCoilPackMsgToShow(quint16 startaddr, quint16 num, QByteArray msg);
    void ReadRegsPackMsgToShow(quint16 startaddr,quint16 num,QByteArray msg);
    //写入寄存器提示
    void WirteRegsPackMsgToShow(quint16 startaddr, quint16 num, QByteArray msg);
    //写入线圈提示
    void WirteCoilPackMsgToShow(quint16 startaddr, quint16 num, QByteArray msg);
};
#endif // MAINWINDOW_H

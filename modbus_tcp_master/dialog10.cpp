#include "dialog10.h"
#include "ui_dialog10.h"

#include <qmessagebox.h>

Dialog10::Dialog10(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog10)
{
    ui->setupUi(this);
    ui->numline->setValidator(new QIntValidator(1, 1968, this));
    ui->addrline->setValidator(new QIntValidator(1,247, this));
    ui->startaddrline->setValidator(new QIntValidator(0, 65535, this));
    ui->over->setDisabled(true);
    inittable();
}

Dialog10::~Dialog10()
{
    delete ui;
}

//获取表格数据
QVector<quint16> Dialog10::getUserInputData()
{
    return bamsg;
}
//表格初始化
void Dialog10::inittable()
{
    //设置表格
    QStringList TableHeader;
    TableHeader << "地址" << "数据";
    ui->tableregs->setHorizontalHeaderLabels(TableHeader);
    ui->tableregs->setEditTriggers(QAbstractItemView::NoEditTriggers);


    //限制从机地址
    connect(ui->addrline,&QLineEdit::textChanged,[=](){
        if(ui->addrline->text().toInt()>247||ui->addrline->text().toInt()<1)
        {
            QMessageBox::warning(this,"提示","请检查起时地址和数量是否合法！",QMessageBox::Ok,QMessageBox::NoButton);
            ui->addrline->clear();
        }
    });


    connect(ui->numline,&QLineEdit::textChanged,[=](){
        if(ui->numline->text().toInt()+ui->startaddrline->text().toInt()>65536)
        {
            QMessageBox::warning(this,"提示","请检查起时地址和数量是否合法！",QMessageBox::Ok,QMessageBox::NoButton);
            ui->numline->clear();
        }
    });

    connect(ui->startaddrline,&QLineEdit::textChanged,[=]{
       if(ui->startaddrline->text().toInt()>65535)
       {
            QMessageBox::warning(this,"提示","请检查起时地址是否合法！",QMessageBox::Ok,QMessageBox::NoButton);
            ui->startaddrline->clear();
       }
    });


    connect(ui->numline,&QLineEdit::textChanged,[=]{
       if(ui->numline->text().toInt()>123||ui->numline->text().toInt()<1)
       {
            QMessageBox::warning(this,"提示","请检查寄存器是否合法！",QMessageBox::Ok,QMessageBox::NoButton);
            ui->numline->clear();
       }
    });

}
//表格数据初始化
void Dialog10::tabledatainit(quint16 BeginAddress,quint16 Number)
{
    ui->tableregs->clear();
    inittable();
    for(quint16 i = BeginAddress,k=0;k<Number; i++,k++)
    {
        //地址设置
        QString adr =  "0x" + QString("%1").arg(i,4,16,QLatin1Char('0'));
        ui->tableregs->setItem(k,0, new QTableWidgetItem(adr.toUpper()));
        ui->tableregs->item(k,0)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableregs->item(k,0)->setFlags(Qt::ItemIsEditable);
        //在线圈数据表中显示数据
        ui->tableregs->setItem(k,1,new QTableWidgetItem(QString("%1").arg(initdata[k])));
        //设置表格内文字水平+垂直对齐
        ui->tableregs->item(k,1)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    }
}
//获取地址信息
QVector<quint16> Dialog10::infdata()
{
    quint16 addr = ui->addrline->text().toInt();
    quint16 num = ui->numline->text().toInt();
    inf.push_back(addr);
    inf.push_back(oldstartaddr);
    inf.push_back(num);
    return inf;
}
//设置完成
void Dialog10::on_over_clicked()
{
    quint16 addr = ui->addrline->text().toInt();
    quint16 num = ui->numline->text().toInt();


    if(num==0||addr==0||ui->start_btn->isEnabled())
    {
        QMessageBox::warning(this,"错误","设置有误，请重新输入！");
    }
    else
    {
        ui->start_btn->setDisabled(false);
        ui->addrline->setDisabled(false);
        ui->numline->setDisabled(false);
        ui->over->setDisabled(true);
        GetData0X10(bamsg,oldstartaddr,num);
    }
}
//封装成字节
void Dialog10::GetData0X10(QVector<quint16> &coilsDataArr,quint16 BeginAddress,quint16 Number)
{
    for(quint16 i = 0; i < Number; i++)
    {
        //读出寄存器数据
        quint16 buffer = (quint16)ui->tableregs->item(i,1)->text().toInt();
        coilsDataArr.push_back(buffer);
    }
    QMessageBox::information(this,"提示","设置完成！");
}
//开始设置
void Dialog10::on_start_btn_clicked()
{
    quint16 satrtaddr = ui->startaddrline->text().toInt();
    oldstartaddr = satrtaddr;
    quint16 num = ui->numline->text().toInt();
    quint16 addr = ui->addrline->text().toInt();
    if(num==0||addr==0)
    {
        QMessageBox::warning(this,"错误","输入有误请先检查从机地址，起始地址和数量！");
    }
    else if(satrtaddr+num-1>65535||num>123)
    {
        QMessageBox::warning(this,"错误","输入有误请先检查起始地址和数量！");
    }
    else
    {
        tabledatainit(satrtaddr,num);
        ui->start_btn->setDisabled(true);
        ui->over->setDisabled(false);

        ui->addrline->setDisabled(true);
        ui->numline->setDisabled(true);
    }
}
//单元格改变
void Dialog10::on_tableregs_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    if(isfc==1)
    {
        pre=edit->text();
    }
    if(isclicked==1)
    {
        isfc=0;
        ui->tableregs->setCellWidget(previousRow, 1,NULL);
        ui->tableregs->setItem(previousRow,1,new QTableWidgetItem(pre));
    }
    isclicked=0;
}
//双击单元格
void Dialog10::on_tableregs_cellDoubleClicked(int row, int column)
{
    QString cur;
    if(!(ui->tableregs->item(row,1)==NULL))
    {
        cur=ui->tableregs->item(row,1)->text();
    }
    else
    {
        cur="0";
    }
    edit=new QLineEdit;
    edit->setText(cur);
    QRegExp rx("^([0-9]|[1-9]\\d|[1-9]\\d{2}|[1-9]\\d{3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5])$");
    QRegExpValidator *pReg = new QRegExpValidator(rx, this);
    edit->setValidator(pReg);
    ui->tableregs->setCellWidget(row,1,edit);
    isfc=1;
    isclicked=1;
}



#ifndef DIALOG10_H
#define DIALOG10_H

#include <QDialog>
#include <QLineEdit>

namespace Ui {
class Dialog10;
}

class Dialog10 : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog10(QWidget *parent = nullptr);
    ~Dialog10();
    QVector<quint16> getUserInputData();

    void GetData0X10(QVector<quint16> &coilsDataArr, quint16 BeginAddress, quint16 Number);
    QVector<quint16> infdata();
private slots:
    void on_tableregs_cellDoubleClicked(int row, int column);
    void on_tableregs_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
    void on_start_btn_clicked();
    void on_over_clicked();
private:
    Ui::Dialog10 *ui;
    QVector<quint16> inf;
    //线圈表格初始化
    quint8 initdata[123]={0};
    QVector<quint16> bamsg;
    quint16 oldstartaddr;
    QRegExp rx;
    QString pre;
    int isfc=0;
    int isclicked=0;
    QLineEdit* edit;
    void tabledatainit(quint16 BeginAddress, quint16 Number);
    void inittable();

};

#endif // DIALOG10_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QByteArray>
#include <QDebug>
#include <QThread>
#include <QTcpSocket>
#include<QStandardItemModel>
#include<QTableWidget>
#include<QTimer>
#include<QDateTime>
#include<QSettings>
#include<QFile>
#include<QFileInfo>
#include<QFileDialog>
#include<QTextStream>
#include <QCloseEvent>
#include<QTcpServer>
#include "analysis.h"
#include "widget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    //主线程初始化
    void modbus_init();
    //信号
    //建立连接
    void creatConnect();
    QString getLocalIp();
    //Widget窗口
    Widget *widget=nullptr;
signals:
    void  Start(QByteArray msg,quint8 m_addr);


public slots:

private slots:
    void table_dataInit();//表格初始化
    void ReadOutIniData();
    void wirtTabler(quint16 num, quint16 satrtaddr, QVector<quint16> bar);
    void wirtTablec(quint16 num, quint16 satrtaddr, QString bac);
    //显示时间
    void showtime();
    //显示错误信息
    void sendBackMsg(QByteArray msg);
    //保存历史数据
    void on_pushButton_clicked();
    void on_save_btn_clicked();
    void fileStart();

    void on_Transfiles_clicked();

private:
    Ui::MainWindow *ui;
//——————变量区————————
    QSettings* readseting=nullptr;
    QTimer * m_timer;
     bool TcpServerConnectState = false;
     bool isConnect = false;
     QString defaultIPAddress = "10.100.14.37";
     quint8 defaultSlaveAddress = 1;
     quint8 SlaveAddress;
     quint16 mb_startaddr,mb_num,wnum;
    analysis * m_analysis;
    quint8 m_addr;
    QString ipAddress;
    quint16 portNum;
    //接收报文
    QByteArray ba;

    QTcpSocket *MySocket;
    QTcpServer *MyServer;

    //存放客户端Ip地址和端口
    qintptr tcpReceiveSocketDescriptor;
    QString ClientIpAddress;
    quint16 ClientPortNumber;

    //存放客户端Ip地址和端口
    QString MasterIpAddress;
    quint16 MasterPortNumber;


//    MyServer* m_tcpServer = nullptr;




//——————函数区————————
    void ShowMsgArray(QByteArray ba);
    void ShowMsgErro(QString str);
    bool AskMessage(QString msg);

    void ShowMsgString(QString error);
    void ShowMsgQByteArray(QByteArray ba);

    void AddTime();
    void DisconnectShowMsg();
    //显示请求报文
    void ShowRequest();
    void ResponseMsg(QByteArray ba);
    void ipDefalut();
    void SlaveAddressDefault();
    void ListenTcpServer();
    void CreateNewConnect();
    void ShowTcpServerDisconnect(QAbstractSocket::SocketError);
};
#endif // MAINWINDOW_H

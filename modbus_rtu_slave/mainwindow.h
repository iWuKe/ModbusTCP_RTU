#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QThread>
#include <qsettings.h>
#include "analysis.h"

#include <QtSerialPort/QSerialPort>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QList<QSerialPortInfo>  uart_info;//串口信息链表

     QSerialPort Uart_Objects;//串口对象

     //界面串口配置参数结构体
     typedef struct portParameters
     {
         //本机地址
         QString localAddress;
         //串口号
         QString portNumber;
         //波特率序号
         qint32 baudRate;
         //数据位序号
         QSerialPort::DataBits dataBits;
         //停止位序号
         QSerialPort::StopBits stopBits;
         //校验位序号
         QSerialPort::Parity parity;
     }portParameters;
     //串口配置参数结构体初始化函数

    QStringList getPortNameList();//获取所有可用的串口列表
    void errorOccurredqq(QSerialPort::SerialPortError error);
    void portStructInitialize(portParameters *structPt);
signals:
    void sendData2Uart(QByteArray);
    void  Start(QByteArray msg,quint8 addr);

    //void readyread(QByteArray);
public slots:
    //读取完向主线程发送信号
        void UART_eerom_SLOTS(int y);

private slots:
    void PortButton();
    void receiveInfo();           //接收函数
    void on_clearButton_clicked();  //清除按钮
    void init();    //主线程初始化
    void tableInit();//表格初始化
    void iniReadDate();//ini数据显示
    void showmsg(QString msg);//显示信息
    void showtime();//显示时间
    void sendBackMsg(QByteArray msg);
    void ShowMsgString(QString error);
    void ShowMsgQByteArray(QByteArray ba);
    void ByteToHexString(QString &str, QByteArray &ba);
    void wirtTablec(quint16 num, quint16 satrtaddr,QString bac);
    void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar);
    void on_pushButton_clicked();
    void timerOut();

    //串口刷新
    void UART_Flush_SLOTS();
    //void UART_CLOSE_SLOTS(bool);
    //void UART_OPEN_SLOTS(bool);
    void on_btnOpenCOM_clicked();

    void on_closebtn_clicked();
    //历史信息保存
    void on_historyBtn_clicked();

private:
    Ui::MainWindow *ui;
    MainWindow *Mw;
    void readyReads();
     bool UART_RUN = false;//软件串口是否打开标志位
    analysis *m_analysis;
    //初始化波特率存储
    int *baudRate;
    int *serialPortStatus= 0;
    bool uartIsOpen = false;
    QSerialPort* m_serialPort; //串口类
    QStringList m_portNameList;
    //当前时间定时器对象
    QTimer * m_timer;
    QTimer *rcvDataTimer;
    QByteArray sendArray;
    quint8 m_addr;
    //ini文件对象
    QSettings* readseting=nullptr; 
    int ReceivingTime();
    void errorOccurred(QString error);
    void openSucess(bool sucess);

    void PortDisConnect();
    void boxDisconnect();
    bool PortConnect(portParameters *structPt);
    void boxConnect();
    void OpenSuccessPrompt();
    void OpenFailurePrompt();
    void handleSerialError(QSerialPort::SerialPortError error);
    void ClosePortPrompt();
};
#endif // MAINWINDOW_H

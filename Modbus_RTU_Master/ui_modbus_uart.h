/********************************************************************************
** Form generated from reading UI file 'modbus_uart.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODBUS_UART_H
#define UI_MODBUS_UART_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_modbus_uart
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_2;
    QLabel *time_line;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_4;
    QComboBox *stop_comboBox;
    QComboBox *Baud_comboBox;
    QComboBox *come_comboBox;
    QHBoxLayout *horizontalLayout_3;
    QCheckBox *write_hex_checkBox;
    QCheckBox *write_CRC16_checkBox;
    QComboBox *data_bit_comboBox;
    QComboBox *verify_comboBox;
    QLabel *sign_label;
    QCheckBox *receive_hex_checkBox;
    QPushButton *historyBtn;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_4;
    QPushButton *Flush_UART;
    QPushButton *close_uart;
    QPushButton *open_uart;
    QCheckBox *AutoSend_checkBox;
    QSpinBox *time_spinBox;
    QLabel *label;
    QSpinBox *receive_time_spinBox;
    QLabel *receive_label;
    QPushButton *receive_pushButton;
    QLabel *send_label;
    QPushButton *send_pushButton;
    QTextEdit *display_TextEdit;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_7;
    QTableWidget *table_coil;
    QWidget *tab_2;
    QGridLayout *gridLayout_6;
    QTableWidget *tableregs;
    QLineEdit *lineEdit_data;
    QPushButton *write_data;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *labelstartaddrees;
    QPushButton *send_msg;
    QPushButton *regsetupbtn;
    QLineEdit *slave_LineEdit;
    QComboBox *GN_combox;
    QLabel *labelGN;
    QLabel *labelslaveaddress;
    QLineEdit *start_ad_LineEdit;
    QPushButton *setupbtn;
    QLineEdit *coil_LineEdit;
    QLabel *labelcoil;
    QPushButton *coilsetepbtn;
    QLineEdit *reg_LineEdit;
    QLabel *labelreg;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *modbus_uart)
    {
        if (modbus_uart->objectName().isEmpty())
            modbus_uart->setObjectName(QString::fromUtf8("modbus_uart"));
        modbus_uart->resize(625, 829);
        centralwidget = new QWidget(modbus_uart);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_2 = new QGridLayout(centralwidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        time_line = new QLabel(centralwidget);
        time_line->setObjectName(QString::fromUtf8("time_line"));
        time_line->setMaximumSize(QSize(300, 16777215));

        gridLayout_2->addWidget(time_line, 0, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(200, 16777215));
        gridLayout_4 = new QGridLayout(groupBox_2);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        stop_comboBox = new QComboBox(groupBox_2);
        stop_comboBox->addItem(QString());
        stop_comboBox->addItem(QString());
        stop_comboBox->addItem(QString());
        stop_comboBox->addItem(QString());
        stop_comboBox->setObjectName(QString::fromUtf8("stop_comboBox"));

        gridLayout_4->addWidget(stop_comboBox, 5, 0, 1, 1);

        Baud_comboBox = new QComboBox(groupBox_2);
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->addItem(QString());
        Baud_comboBox->setObjectName(QString::fromUtf8("Baud_comboBox"));

        gridLayout_4->addWidget(Baud_comboBox, 2, 0, 1, 1);

        come_comboBox = new QComboBox(groupBox_2);
        come_comboBox->setObjectName(QString::fromUtf8("come_comboBox"));
        come_comboBox->setMaximumSize(QSize(200, 16777215));

        gridLayout_4->addWidget(come_comboBox, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        write_hex_checkBox = new QCheckBox(groupBox_2);
        write_hex_checkBox->setObjectName(QString::fromUtf8("write_hex_checkBox"));
        write_hex_checkBox->setChecked(true);

        horizontalLayout_3->addWidget(write_hex_checkBox);

        write_CRC16_checkBox = new QCheckBox(groupBox_2);
        write_CRC16_checkBox->setObjectName(QString::fromUtf8("write_CRC16_checkBox"));
        write_CRC16_checkBox->setChecked(true);

        horizontalLayout_3->addWidget(write_CRC16_checkBox);


        gridLayout_4->addLayout(horizontalLayout_3, 7, 0, 1, 1);

        data_bit_comboBox = new QComboBox(groupBox_2);
        data_bit_comboBox->addItem(QString());
        data_bit_comboBox->addItem(QString());
        data_bit_comboBox->addItem(QString());
        data_bit_comboBox->addItem(QString());
        data_bit_comboBox->setObjectName(QString::fromUtf8("data_bit_comboBox"));

        gridLayout_4->addWidget(data_bit_comboBox, 3, 0, 1, 1);

        verify_comboBox = new QComboBox(groupBox_2);
        verify_comboBox->addItem(QString());
        verify_comboBox->addItem(QString());
        verify_comboBox->addItem(QString());
        verify_comboBox->addItem(QString());
        verify_comboBox->addItem(QString());
        verify_comboBox->addItem(QString());
        verify_comboBox->setObjectName(QString::fromUtf8("verify_comboBox"));

        gridLayout_4->addWidget(verify_comboBox, 4, 0, 1, 1);

        sign_label = new QLabel(groupBox_2);
        sign_label->setObjectName(QString::fromUtf8("sign_label"));
        sign_label->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(sign_label, 0, 0, 1, 1);

        receive_hex_checkBox = new QCheckBox(groupBox_2);
        receive_hex_checkBox->setObjectName(QString::fromUtf8("receive_hex_checkBox"));
        receive_hex_checkBox->setChecked(true);

        gridLayout_4->addWidget(receive_hex_checkBox, 6, 0, 1, 1);

        historyBtn = new QPushButton(groupBox_2);
        historyBtn->setObjectName(QString::fromUtf8("historyBtn"));

        gridLayout_4->addWidget(historyBtn, 8, 0, 1, 1);


        verticalLayout_3->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(centralwidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMaximumSize(QSize(200, 16777215));
        verticalLayout_4 = new QVBoxLayout(groupBox_3);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        Flush_UART = new QPushButton(groupBox_3);
        Flush_UART->setObjectName(QString::fromUtf8("Flush_UART"));

        verticalLayout_4->addWidget(Flush_UART);

        close_uart = new QPushButton(groupBox_3);
        close_uart->setObjectName(QString::fromUtf8("close_uart"));

        verticalLayout_4->addWidget(close_uart);

        open_uart = new QPushButton(groupBox_3);
        open_uart->setObjectName(QString::fromUtf8("open_uart"));

        verticalLayout_4->addWidget(open_uart);

        AutoSend_checkBox = new QCheckBox(groupBox_3);
        AutoSend_checkBox->setObjectName(QString::fromUtf8("AutoSend_checkBox"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AutoSend_checkBox->sizePolicy().hasHeightForWidth());
        AutoSend_checkBox->setSizePolicy(sizePolicy);

        verticalLayout_4->addWidget(AutoSend_checkBox);

        time_spinBox = new QSpinBox(groupBox_3);
        time_spinBox->setObjectName(QString::fromUtf8("time_spinBox"));
        time_spinBox->setEnabled(true);
        time_spinBox->setMouseTracking(false);
        time_spinBox->setMaximum(999999999);
        time_spinBox->setSingleStep(10);
        time_spinBox->setValue(1000);
        time_spinBox->setDisplayIntegerBase(10);

        verticalLayout_4->addWidget(time_spinBox);

        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_4->addWidget(label);

        receive_time_spinBox = new QSpinBox(groupBox_3);
        receive_time_spinBox->setObjectName(QString::fromUtf8("receive_time_spinBox"));
        receive_time_spinBox->setEnabled(true);
        receive_time_spinBox->setMouseTracking(false);
        receive_time_spinBox->setMaximum(999999999);
        receive_time_spinBox->setSingleStep(10);
        receive_time_spinBox->setValue(100);
        receive_time_spinBox->setDisplayIntegerBase(10);

        verticalLayout_4->addWidget(receive_time_spinBox);


        verticalLayout_3->addWidget(groupBox_3);


        gridLayout_2->addLayout(verticalLayout_3, 0, 4, 4, 1);

        receive_label = new QLabel(centralwidget);
        receive_label->setObjectName(QString::fromUtf8("receive_label"));

        gridLayout_2->addWidget(receive_label, 1, 0, 1, 1);

        receive_pushButton = new QPushButton(centralwidget);
        receive_pushButton->setObjectName(QString::fromUtf8("receive_pushButton"));

        gridLayout_2->addWidget(receive_pushButton, 1, 1, 1, 1);

        send_label = new QLabel(centralwidget);
        send_label->setObjectName(QString::fromUtf8("send_label"));

        gridLayout_2->addWidget(send_label, 1, 2, 1, 1);

        send_pushButton = new QPushButton(centralwidget);
        send_pushButton->setObjectName(QString::fromUtf8("send_pushButton"));

        gridLayout_2->addWidget(send_pushButton, 1, 3, 1, 1);

        display_TextEdit = new QTextEdit(centralwidget);
        display_TextEdit->setObjectName(QString::fromUtf8("display_TextEdit"));
        display_TextEdit->setEnabled(true);
        display_TextEdit->setMaximumSize(QSize(16777215, 500));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        display_TextEdit->setFont(font);

        gridLayout_2->addWidget(display_TextEdit, 2, 0, 1, 4);

        groupBox_4 = new QGroupBox(centralwidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        tabWidget = new QTabWidget(groupBox_4);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_7 = new QGridLayout(tab);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        table_coil = new QTableWidget(tab);
        if (table_coil->columnCount() < 65536)
            table_coil->setColumnCount(65536);
        if (table_coil->rowCount() < 2)
            table_coil->setRowCount(2);
        table_coil->setObjectName(QString::fromUtf8("table_coil"));
        table_coil->setRowCount(2);
        table_coil->setColumnCount(65536);

        gridLayout_7->addWidget(table_coil, 1, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_6 = new QGridLayout(tab_2);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        tableregs = new QTableWidget(tab_2);
        if (tableregs->columnCount() < 65536)
            tableregs->setColumnCount(65536);
        if (tableregs->rowCount() < 2)
            tableregs->setRowCount(2);
        tableregs->setObjectName(QString::fromUtf8("tableregs"));
        tableregs->setRowCount(2);
        tableregs->setColumnCount(65536);

        gridLayout_6->addWidget(tableregs, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout_3->addWidget(tabWidget, 0, 0, 1, 1);


        gridLayout_2->addWidget(groupBox_4, 4, 0, 1, 5);

        lineEdit_data = new QLineEdit(centralwidget);
        lineEdit_data->setObjectName(QString::fromUtf8("lineEdit_data"));

        gridLayout_2->addWidget(lineEdit_data, 5, 0, 1, 4);

        write_data = new QPushButton(centralwidget);
        write_data->setObjectName(QString::fromUtf8("write_data"));

        gridLayout_2->addWidget(write_data, 5, 4, 1, 1);

        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        labelstartaddrees = new QLabel(groupBox);
        labelstartaddrees->setObjectName(QString::fromUtf8("labelstartaddrees"));

        gridLayout->addWidget(labelstartaddrees, 2, 0, 1, 1);

        send_msg = new QPushButton(groupBox);
        send_msg->setObjectName(QString::fromUtf8("send_msg"));

        gridLayout->addWidget(send_msg, 9, 2, 1, 1);

        regsetupbtn = new QPushButton(groupBox);
        regsetupbtn->setObjectName(QString::fromUtf8("regsetupbtn"));

        gridLayout->addWidget(regsetupbtn, 7, 2, 1, 1);

        slave_LineEdit = new QLineEdit(groupBox);
        slave_LineEdit->setObjectName(QString::fromUtf8("slave_LineEdit"));

        gridLayout->addWidget(slave_LineEdit, 1, 2, 1, 1);

        GN_combox = new QComboBox(groupBox);
        GN_combox->addItem(QString());
        GN_combox->addItem(QString());
        GN_combox->addItem(QString());
        GN_combox->addItem(QString());
        GN_combox->setObjectName(QString::fromUtf8("GN_combox"));
        GN_combox->setEditable(false);

        gridLayout->addWidget(GN_combox, 0, 2, 1, 1);

        labelGN = new QLabel(groupBox);
        labelGN->setObjectName(QString::fromUtf8("labelGN"));

        gridLayout->addWidget(labelGN, 0, 0, 1, 1);

        labelslaveaddress = new QLabel(groupBox);
        labelslaveaddress->setObjectName(QString::fromUtf8("labelslaveaddress"));

        gridLayout->addWidget(labelslaveaddress, 1, 0, 1, 1);

        start_ad_LineEdit = new QLineEdit(groupBox);
        start_ad_LineEdit->setObjectName(QString::fromUtf8("start_ad_LineEdit"));

        gridLayout->addWidget(start_ad_LineEdit, 2, 2, 1, 1);

        setupbtn = new QPushButton(groupBox);
        setupbtn->setObjectName(QString::fromUtf8("setupbtn"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(setupbtn->sizePolicy().hasHeightForWidth());
        setupbtn->setSizePolicy(sizePolicy1);
        setupbtn->setMaximumSize(QSize(200, 16777215));

        gridLayout->addWidget(setupbtn, 5, 2, 1, 1);

        coil_LineEdit = new QLineEdit(groupBox);
        coil_LineEdit->setObjectName(QString::fromUtf8("coil_LineEdit"));

        gridLayout->addWidget(coil_LineEdit, 3, 2, 1, 1);

        labelcoil = new QLabel(groupBox);
        labelcoil->setObjectName(QString::fromUtf8("labelcoil"));

        gridLayout->addWidget(labelcoil, 3, 0, 1, 1);

        coilsetepbtn = new QPushButton(groupBox);
        coilsetepbtn->setObjectName(QString::fromUtf8("coilsetepbtn"));

        gridLayout->addWidget(coilsetepbtn, 6, 2, 1, 1);

        reg_LineEdit = new QLineEdit(groupBox);
        reg_LineEdit->setObjectName(QString::fromUtf8("reg_LineEdit"));

        gridLayout->addWidget(reg_LineEdit, 4, 2, 1, 1);

        labelreg = new QLabel(groupBox);
        labelreg->setObjectName(QString::fromUtf8("labelreg"));

        gridLayout->addWidget(labelreg, 4, 0, 1, 1);


        gridLayout_2->addWidget(groupBox, 3, 0, 1, 4);

        modbus_uart->setCentralWidget(centralwidget);
        menubar = new QMenuBar(modbus_uart);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 625, 26));
        modbus_uart->setMenuBar(menubar);
        statusbar = new QStatusBar(modbus_uart);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        modbus_uart->setStatusBar(statusbar);

        retranslateUi(modbus_uart);

        tabWidget->setCurrentIndex(0);
        GN_combox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(modbus_uart);
    } // setupUi

    void retranslateUi(QMainWindow *modbus_uart)
    {
        modbus_uart->setWindowTitle(QCoreApplication::translate("modbus_uart", "Modbus_UART", nullptr));
        time_line->setText(QCoreApplication::translate("modbus_uart", "date", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("modbus_uart", "\351\205\215\347\275\256\344\270\262\345\217\243", nullptr));
        stop_comboBox->setItemText(0, QCoreApplication::translate("modbus_uart", "1  \345\201\234\346\255\242\344\275\215", nullptr));
        stop_comboBox->setItemText(1, QCoreApplication::translate("modbus_uart", "1.5\345\201\234\346\255\242\344\275\215", nullptr));
        stop_comboBox->setItemText(2, QCoreApplication::translate("modbus_uart", "2  \345\201\234\346\255\242\344\275\215", nullptr));
        stop_comboBox->setItemText(3, QCoreApplication::translate("modbus_uart", "\346\234\252\347\237\245\345\201\234\346\255\242\344\275\215", nullptr));

        Baud_comboBox->setItemText(0, QCoreApplication::translate("modbus_uart", "115200", nullptr));
        Baud_comboBox->setItemText(1, QCoreApplication::translate("modbus_uart", "57600", nullptr));
        Baud_comboBox->setItemText(2, QCoreApplication::translate("modbus_uart", "38400", nullptr));
        Baud_comboBox->setItemText(3, QCoreApplication::translate("modbus_uart", "19200", nullptr));
        Baud_comboBox->setItemText(4, QCoreApplication::translate("modbus_uart", "9600", nullptr));
        Baud_comboBox->setItemText(5, QCoreApplication::translate("modbus_uart", "4800", nullptr));
        Baud_comboBox->setItemText(6, QCoreApplication::translate("modbus_uart", "2400", nullptr));
        Baud_comboBox->setItemText(7, QCoreApplication::translate("modbus_uart", "1200", nullptr));

        write_hex_checkBox->setText(QCoreApplication::translate("modbus_uart", "HEX", nullptr));
        write_CRC16_checkBox->setText(QCoreApplication::translate("modbus_uart", "CRC16\346\240\241\351\252\214", nullptr));
        data_bit_comboBox->setItemText(0, QCoreApplication::translate("modbus_uart", "\346\225\260\346\215\256\344\275\215_8", nullptr));
        data_bit_comboBox->setItemText(1, QCoreApplication::translate("modbus_uart", "\346\225\260\346\215\256\344\275\215_7", nullptr));
        data_bit_comboBox->setItemText(2, QCoreApplication::translate("modbus_uart", "\346\225\260\346\215\256\344\275\215_6", nullptr));
        data_bit_comboBox->setItemText(3, QCoreApplication::translate("modbus_uart", "\346\225\260\346\215\256\344\275\215_5", nullptr));

        verify_comboBox->setItemText(0, QCoreApplication::translate("modbus_uart", "\346\262\241\346\234\211\346\240\241\351\252\214", nullptr));
        verify_comboBox->setItemText(1, QCoreApplication::translate("modbus_uart", "\345\201\266\346\240\241\351\252\214", nullptr));
        verify_comboBox->setItemText(2, QCoreApplication::translate("modbus_uart", "\345\245\207\346\240\241\351\252\214", nullptr));
        verify_comboBox->setItemText(3, QCoreApplication::translate("modbus_uart", "0_\346\240\241\351\252\214", nullptr));
        verify_comboBox->setItemText(4, QCoreApplication::translate("modbus_uart", "1_\346\240\241\351\252\214", nullptr));
        verify_comboBox->setItemText(5, QCoreApplication::translate("modbus_uart", "\346\234\252\347\237\245\346\240\241\351\252\214", nullptr));

        sign_label->setText(QCoreApplication::translate("modbus_uart", "\344\270\262\345\217\243\346\234\252\346\211\223\345\274\200", nullptr));
        receive_hex_checkBox->setText(QCoreApplication::translate("modbus_uart", " \346\216\245\346\224\266_HEX", nullptr));
        historyBtn->setText(QCoreApplication::translate("modbus_uart", "\344\277\235\345\255\230\345\216\206\345\217\262\350\256\260\345\275\225", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("modbus_uart", "\344\270\262\345\217\243\346\216\247\345\210\266", nullptr));
        Flush_UART->setText(QCoreApplication::translate("modbus_uart", "\345\210\267\346\226\260\344\270\262\345\217\243", nullptr));
        close_uart->setText(QCoreApplication::translate("modbus_uart", "\345\205\263\351\227\255\344\270\262\345\217\243", nullptr));
        open_uart->setText(QCoreApplication::translate("modbus_uart", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        AutoSend_checkBox->setText(QCoreApplication::translate("modbus_uart", "\350\207\252\345\212\250\345\217\221\351\200\201", nullptr));
        time_spinBox->setSuffix(QString());
        label->setText(QCoreApplication::translate("modbus_uart", "\346\216\245\346\224\266\345\273\266\346\227\266\357\274\232", nullptr));
        receive_time_spinBox->setSuffix(QString());
        receive_label->setText(QCoreApplication::translate("modbus_uart", "\346\216\245\346\224\266\345\255\227\350\212\202\357\274\2320", nullptr));
        receive_pushButton->setText(QCoreApplication::translate("modbus_uart", "\346\270\205\351\233\266", nullptr));
        send_label->setText(QCoreApplication::translate("modbus_uart", "\345\217\221\351\200\201\345\255\227\350\212\202\357\274\2320", nullptr));
        send_pushButton->setText(QCoreApplication::translate("modbus_uart", "\346\270\205\351\233\266", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("modbus_uart", "\346\230\276\347\244\272", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("modbus_uart", "\347\272\277\345\234\210", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("modbus_uart", "\345\257\204\345\255\230\345\231\250", nullptr));
        write_data->setText(QCoreApplication::translate("modbus_uart", "\345\217\221\351\200\201\346\225\260\346\215\256", nullptr));
        groupBox->setTitle(QCoreApplication::translate("modbus_uart", "\350\256\276\347\275\256", nullptr));
        labelstartaddrees->setText(QCoreApplication::translate("modbus_uart", "\350\265\267\346\227\266\345\234\260\345\235\200\357\274\232", nullptr));
        send_msg->setText(QCoreApplication::translate("modbus_uart", "\345\217\221\351\200\201", nullptr));
        regsetupbtn->setText(QCoreApplication::translate("modbus_uart", "10\345\217\202\346\225\260\350\256\276\347\275\256", nullptr));
        slave_LineEdit->setPlaceholderText(QCoreApplication::translate("modbus_uart", "\350\257\267\350\276\223\345\205\245\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232(1-247)", nullptr));
        GN_combox->setItemText(0, QCoreApplication::translate("modbus_uart", "01", nullptr));
        GN_combox->setItemText(1, QCoreApplication::translate("modbus_uart", "03", nullptr));
        GN_combox->setItemText(2, QCoreApplication::translate("modbus_uart", "10", nullptr));
        GN_combox->setItemText(3, QCoreApplication::translate("modbus_uart", "0F", nullptr));

        GN_combox->setCurrentText(QCoreApplication::translate("modbus_uart", "01", nullptr));
        labelGN->setText(QCoreApplication::translate("modbus_uart", "\345\212\237\350\203\275\347\240\201\357\274\232", nullptr));
        labelslaveaddress->setText(QCoreApplication::translate("modbus_uart", "\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232", nullptr));
        start_ad_LineEdit->setPlaceholderText(QCoreApplication::translate("modbus_uart", "\350\257\267\350\276\223\345\205\245\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232(0-65535)", nullptr));
        setupbtn->setText(QCoreApplication::translate("modbus_uart", "\345\217\221\351\200\201", nullptr));
        coil_LineEdit->setPlaceholderText(QCoreApplication::translate("modbus_uart", "\350\257\267\350\276\223\345\205\245\347\272\277\345\234\210\346\225\260\351\207\217(1-2000)", nullptr));
        labelcoil->setText(QCoreApplication::translate("modbus_uart", "\347\272\277\345\234\210\344\270\252\346\225\260:", nullptr));
        coilsetepbtn->setText(QCoreApplication::translate("modbus_uart", "0F\345\217\202\346\225\260\350\256\276\347\275\256", nullptr));
        reg_LineEdit->setPlaceholderText(QCoreApplication::translate("modbus_uart", "\345\257\204\345\255\230\345\231\250\346\225\260\351\207\217(1-125)", nullptr));
        labelreg->setText(QCoreApplication::translate("modbus_uart", "\345\257\204\345\255\230\345\231\250\344\270\252\346\225\260\357\274\232", nullptr));
    } // retranslateUi

};

namespace Ui {
    class modbus_uart: public Ui_modbus_uart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODBUS_UART_H

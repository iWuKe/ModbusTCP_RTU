# 基于Qt的Modbus主从站

#### 介绍
该项目是用Qt实现的Modbus主从站

#### 使用说明

1.  学习文档
2.  方案书
3.  RTU主站
4.  RTU从站
5.  TCP主站
6.  TCP从站
7.  TCP主站优化 文件传输 下载
8.  TCP从站优化 多线程 文件传输

#### 参与贡献

1.  Fork 本仓库
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
